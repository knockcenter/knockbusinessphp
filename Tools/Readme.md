Knock Probes
================

Some tools to help in dev mode

# Listener python
This software is for dev usage only.

```bash
$ python Tools/listener.py
INFO:listener:this software is for mock use only.
INFO:listener:Ready to receive datagram on /tmp/mock.knockdaemon.socket

```

This software trace every frames received and parse all items inside.
It's helpful in unit test.
