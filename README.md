Knock Probes
================

[![Build Status](https://app.codeship.com/projects/35e386e0-96c1-0134-6668-7e4be53ab501/status?branch=master)](https://app.codeship.com/projects/186972)

Latest release: [0.99.0-RC2](https://packagist.org/packages/knock/probes)

PHP >= 5.6.0

This library contains a class to send business probe to knock daemon.

https://knock.center

Copyright (C) 2013/2014/2015/2016 Laurent Champagnac / Laurent Labatut

# Source code 


* We use a right margin of 360 characters (please don't talk me about 80 chars)
* SDK code is located inside "./src"
* Test are located inside "./tests"
* All test files must end with `Test`
* All tests must adapt to any running directory
* We are still bound to php 5.6 2.7 (we will move to php 7 later on)
* We are using docstring.

# Requirements


- Knockdaemon
- An account on [Knock](https://knock.center)


Authors
-------

* [Laurent Champagnac](lchampagnac@knock.center)
* [Laurent Labatut](llabatut@knock.center)
* [The Community Contributors]

Contribute
----------

Contributions to the package are always welcome!

* Report any bugs or issues you find on the [issue tracker](https://bitbucket.org/knockcenter/knockbusinessphp/issues).
* You can grab the source code at the package's [Bitbucket repository](https://bitbucket.org/knockcenter/knockbusinessphp.git).

Support
-------

If you are having problems, open a ticket on [issue tracker](https://bitbucket.org/knockcenter/knockbusinessphp/issues). 

License
-------

All contents of this package are licensed under the [GPL V2](https://www.gnu.org/licenses/old-licenses/gpl-2.0.en.html).



# Example

## Installation


Use [Composer] to install the package:

```bash
$ composer require knock/probes
```

## Push a Gauge Probe
You can push a gauge probe one by one without declare first.
```php
<?php
 
 require __DIR__ . '/vendor/autoload.php';
 use Knock\Probes\Knock;
 
 $k = new Knock();
 $k->gauge('apple', 2);
 $k->gauge('orange', 3);


```

## Push a Counter Probe
You can push a incerment probe.
```php
<?php

require __DIR__ . '/vendor/autoload.php';
use Knock\Probes\Knock;

$k = new Knock();
$k->increment('action1', 1);
// do something

```

## Delay to Count

Delay to count is a spécial probe. This probe agregate all execution time in a dict of range of time.

```php
<?php

require __DIR__ . '/vendor/autoload.php';
use Knock\Probes\Knock;

$k = new Knock();
$timecode = $k->start_delay('api_facebook_request');
sleep(2); // here your external request
$k->stop_delay($timecode);

```

# Important information

Knock lib have a destructor. So for performance improvement you can omit to commit.
So it's not possible to uncommit a probe.

--------

This program is free software; you can redistribute it and/or
modify it under the terms of the GNU General Public License
as published by the Free Software Foundation; either version 2
of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
