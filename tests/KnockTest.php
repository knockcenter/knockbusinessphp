<?php


require __DIR__ . '/../vendor/autoload.php';
use Knock\Probes\Knock;


/**
 * Created by laurent labatut.
 * Date: 11/27/16
 * Time: 3:27 AM
 */
class KnockTest extends PHPUnit_Framework_TestCase
{
    public function testGlobal()
    {
        $r = new Knock();
        $timecode1 = $r->start_delay('delay1');
        $this->assertTrue($r->increment('counter1', 1));
        $this->assertTrue($r->increment('counter2', 1));
        $this->assertTrue($r->increment('counter2', 1));
        $this->assertTrue($r->gauge('gauge1', 1));
        $this->assertTrue($r->gauge('gauge1', 2));
        $this->assertTrue($r->stop_delay($timecode1));

        $nb = $r->commit();
        $this->assertEquals(4, $nb);
    }

    public function testGlobalToPythonMock()
    {
        $r = new Knock('/tmp/mock.knockdaemon.socket');
        $i = 0;
        while ($i++ < 30000) {

            $timecode1 = $r->start_delay('delay1');
            $this->assertTrue($r->increment('counter1', 1));
            $this->assertTrue($r->increment('counter2', 1));
            $this->assertTrue($r->increment('counter2', 1));
            $this->assertTrue($r->gauge('gauge1', 1));
            $this->assertTrue($r->gauge('gauge1', 2));
            $this->assertTrue($r->stop_delay($timecode1));

        }
        $nb = $r->commit();
        $this->assertEquals(30003, $nb);
    }
}
