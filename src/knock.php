<?php

namespace Knock\Probes;

use Exception;

class Knock
{

    function __construct($socket_file = null)
    {
        $this->counters = array();
        $this->gauges = array();
        $this->delays = array();
        if ($socket_file == null) {
            $socket_file = '/var/run/knockdaemon.udp.socket';
        }
        $this->socket_file = $socket_file;
    }

    /**
     * @param $item : item Name
     * @param $value
     * @return bool
     */
    public function increment($item, $value)
    {
        if (!$this->is_clean_item($item)) {
            return false;
        }
        if (isset($this->counters[$item])) {
            $this->counters[$item] += $value;
        } else {
            $this->counters[$item] = $value;
        }
        return true;
    }

    /**
     * Add or replace an item
     *
     * @param $item
     * @param $value
     * @return bool
     */
    public function gauge($item, $value)
    {
        if (!$this->is_clean_item($item)) {
            return false;
        }
        $this->gauges[$item] = $value;
        return true;
    }

    /**
     *
     * Start a delay probe, you need to call stop after
     *
     * @param $item
     * @return array timecode
     */
    public function start_delay($item)
    {
        return [$item, microtime(true)];
    }

    /**
     *
     * Add delay to count probe
     *
     * @param $timecode
     * @return bool
     */
    public function stop_delay($timecode)
    {

        $item = $timecode[0];
        if (!$this->is_clean_item($item)) {
            return false;
        }
        $time = (microtime(true) - $timecode[1]) * 1000;
        $this->delays[] = [$item, $time];
        return true;
    }

    /**
     * Commit all items
     * return number of item commited or false
     *
     * @return mixed
     */
    public function commit()
    {

        $list_item = array();
        while ($item = array_slice($this->counters, -1, 1, true)) {

            $item_name = key($item);
            $value = $item[$item_name];
            unset($this->counters[$item_name]);
            $list_item[] = [$item_name, 'C', $value];
        }

        while ($item = array_slice($this->gauges, -1, 1, true)) {

            $item_name = key($item);
            $value = $item[$item_name];
            unset($this->gauges[$item_name]);
            $list_item[] = [$item_name, 'G', $value];
        }

        while ($item = array_pop($this->delays)) {

            $item_name = $item[0];
            $value = $item[1];
            unset($this->delays[$item_name]);
            $list_item[] = [$item_name, 'DTC', $value];
        }

        if (count($list_item) === 0) {
            return false;
        }
        try {
            foreach (array_chunk($list_item, 704) as $chunk) {
                $json_list = json_encode($chunk);
                // Json encode all items
                $this->send($json_list);
            }
        } catch (Exception $e) {
            return false;
        }
        return count($list_item);
    }

    /**
     * Send all item to knockdaemon
     *
     * @param $json_items
     * @return bool
     */
    private function send($json_items)
    {
        // send to knockdaemon
        $sock = socket_create(AF_UNIX, SOCK_DGRAM, 0);
        if (!$sock) {
            return false;
        }
        try {
            socket_sendto($sock, $json_items, strlen($json_items), 0, $this->socket_file);
            socket_close($sock);
            return true;
        } catch (Exception $e) {
            return false;
        }
    }

    /**
     * @param $item
     * @return bool
     */
    private function is_clean_item($item)
    {
        if (strlen($item) > 70) {
            return false;
        } else {
            return true;
        }
    }

    function __destruct()
    {
        $this->commit();
    }
}